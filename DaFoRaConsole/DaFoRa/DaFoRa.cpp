// DaFoRa.cpp : Defines the entry point for the console application.
// The AI living in 501 B.C.

#include "stdafx.h"
#include <cstdio>
#include <iostream>
#include <stdlib.h>
#include <cstring>
#include <algorithm>
#include <string>
#include <fstream>
#include <ctime>
#include <Windows.h>
#include <vector>
#include "C:\Program Files\Python36\include\Python.h"

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

using namespace std;


int main()
{
	ifstream GetName ("More Data/UserName.txt", ifstream::in);
	string InfileName;
	getline(GetName, InfileName);
	GetName.close();

	ifstream GetAge("More Data/UserAge.txt", ifstream::in);
	string InfileAge;
	getline(GetAge, InfileAge);
	GetAge.close();

	if (InfileName != "" || InfileAge != "") {

		cout << "I recognize you! You are " << InfileName << "! I also happen to know that you are " << InfileAge << " years old!" << endl;

	}
	else {

		//Preps the UserName file for saving data
		fstream LoadName;
		LoadName.open("More Data/UserName.txt");

		// DaFoRa greets and asks your name and saves it to file
		cout << "Hello there, I am DaFoRa. What is your name?" << endl;
		string NewGreeting1 = "PythonVoiceCode/NewGreeting1.py";
		string command = "python ";
		command += NewGreeting1;
		system(command.c_str());
		char UserName[128];
		cin.getline(UserName, 128);
		LoadName << UserName;
		LoadName.close();


		// Preps the UserAge file for saving data
		fstream LoadAge;
		LoadAge.open("More Data/UserAge.txt");

		// It then ask your age and saves it to file
		cout << "Hello " << UserName << ", and how old are you?" << endl;
		string NewGreeting2 = "PythonVoiceCode/NewGreeting2.py";
		string command2 = "python ";
		command2 += NewGreeting2;
		system(command2.c_str());
		char UserAge[8];
		cin.getline(UserAge, 8);
		LoadAge << UserAge;
		LoadAge.close();


		// Then concluding Statement and AI task
		cout << "Ahh, " << UserAge << ", that's a nice age to be. Isn't it, " << UserName << "?" << endl;
	};
	
	// Any AI function must be in this loop below
	for (;;) {

		// This will recall the name from file
		ifstream GetName;
		GetName.open("More Data/UserName.txt", ifstream::in);
		string UserName;
		getline(GetName, UserName);

		// Now Starts The Assitant part (it's looped)
		cout << "What can I do for you today, " << UserName << "? I can do all 4 basic mathematical operations, tell time, take a note and tell you the weather! Type close if you want me to go away :(. Thats all I can do for now!" << endl;
		string WhatToDo = "PythonVoiceCode/WhatToDoNow.py";
		string command = "python ";
		command += WhatToDo;
		system(command.c_str());
		string UserTask;
		getline(cin, UserTask);
		transform(UserTask.begin(), UserTask.end(), UserTask.begin(), ::tolower);



		// Addition Code
		if (UserTask == "add" || (UserTask == "addition")) {
			cout << "Tell me the two numbers you'd like to add." << endl;
			string Add = "PythonVoiceCode/Adding.py";
			string command = "python ";
			command += Add;
			system(command.c_str());
			int a;
			int b;
			cin >> a >> b;
			cout << "The answer is: " << a + b << endl;
			cin.ignore();

		}

		// Subtraction Code
		else if (UserTask == "subtract" || (UserTask == "subtraction")) {
			cout << "Tell me the two numbers you'd like to subtract." << endl;
			string minus = "PythonVoiceCode/Subtraction.py";
			string command = "python ";
			command += minus;
			system(command.c_str());
			int a;
			cin >> a;
			int b;
			cin >> b;
			cout << "The answer is: " << a - b << endl;
			cin.ignore();

		}

		// Multiplication Code
		else if (UserTask == "multiply" || (UserTask == "multiplication")) {
			cout << "Tell me the two numbers you'd like to multiply." << endl;
			string Times = "PythonVoiceCode/Multiplication.py";
			string command = "python ";
			command += Times;
			system(command.c_str());
			int a;
			cin >> a;
			int b;
			cin >> b;
			cout << "The answer is: " << a * b << endl;
			cin.ignore();
		}

		// Division Code
		else if (UserTask == "divide" || (UserTask == "division")) {
			cout << "Tell me the two numbers you'd like to divide." << endl;
			string divide = "PythonVoiceCode/Divide.py";
			string command = "python ";
			command += divide;
			system(command.c_str());
			int a;
			cin >> a;
			int b;
			cin >> b;
			cout << "The answer is: " << a / b << endl;
			cin.ignore();
		}

		//This closes the program on enter/return key press
		else if (UserTask == "close") {
			cout << "It was nice talking to you in this demo, " << UserName << ". Now press the enter key and I'll whisk away!";
			string leave = "PythonVoiceCode/Closing.py";
			string command = "python ";
			command += leave;
			system(command.c_str());
			cin.ignore();
			cout << "Adios, " << UserName << ".";
			Sleep(500);
			return 0;
		}

		//Allows DaFoRa to relay the current system time and date
		else if (UserTask == "time") {
			time_t now = time(0);
			char* dt = ctime(&now);
			cout << "It is: " << dt << endl;
			string time = "PythonVoiceCode/TellTime.py";
			string command = "python ";
			command += time;
			system(command.c_str());

		}

		// Taking Notes
		else if (UserTask == "notes" || (UserTask == "note")) {
			cout << "Would you like to continue an existing note or start a new one?" << endl;
			string UserNoteChoice;
			getline(cin, UserNoteChoice);
			transform(UserNoteChoice.begin(), UserNoteChoice.end(), UserNoteChoice.begin(), ::tolower);

			// To continue an old Note
			if (UserNoteChoice == "continue" || UserNoteChoice == "existing") {
				//Somehow display all notes
				string filename = "PythonCode/NotesDirectorySearcher.py";
				string command = "python ";
				command += filename;
				system(command.c_str());

				//Title Of Existing Note
				cout << "Which note would you like to open?" << endl;
				
				string OldNoteTitle;
				getline(cin, OldNoteTitle);
				string NoteTitle = "Notes/" + OldNoteTitle + ".txt";

				if (ifstream LoadUserNoteTemp{ NoteTitle, ios::binary | ios::ate }) {
					auto size = LoadUserNoteTemp.tellg();
					string str(size, '\0'); // construct string to stream size
					LoadUserNoteTemp.seekg(0);
					if (LoadUserNoteTemp.read(&str[0], size)) {
						cout << str << '\n';
					};
				};
				
				//If User Would Like to edit the note
				cout << "Would you like to edit this document?" << endl;
				string UserEditNoteChoice;
				getline(cin, UserEditNoteChoice);
				transform(UserEditNoteChoice.begin(), UserEditNoteChoice.end(), UserEditNoteChoice.begin(), ::tolower);

				if (UserEditNoteChoice == "yes") {
					string filename = "./Documents/DaFoRa/DaFoRaConsole/DaFoRa/Notes/" + OldNoteTitle + ".txt";
					string command = "start ";
					command += filename;
					system(command.c_str());
				};
			}
			

			// Make a New Note
			else if (UserNoteChoice == "create" || UserNoteChoice == "new" || UserNoteChoice == "start") {
				// Title Of The Note
				cout << "Title your note:";

				string TempNoteTitle;
				getline(cin, TempNoteTitle);

				string NoteTitle = "Notes/" + TempNoteTitle + ".txt";

				//Opening said Note File
				fstream LoadUserNoteTemp;
				LoadUserNoteTemp.open(NoteTitle, ios::out);

				// To type in the file
				cout << "Type your note here:" << endl;;
				string UserNoteTemp;
				getline(cin, UserNoteTemp);
				LoadUserNoteTemp << UserNoteTemp;
				LoadUserNoteTemp.close();
			}

			else {
				cout << "I don't get it. Try again?" << endl;
				string idk = "PythonVoiceCode/IdkTryAgain.py";
				string command = "python ";
				command += idk;
				system(command.c_str());
			};
		}

		// Weather Code Python Wrapper
		else if (UserTask == "weather") {
			cout << "Hold On the Weather is coming!" << endl;
			string filename = "PythonCode/GetCurrentWeather.py";
			string command = "python ";
			command += filename;
			system(command.c_str());

		}

		// User didn't enter a function DaFoRa could do
		else {
			cout << "I don't get it. Try again?" << endl;
			string idk = "PythonVoiceCode/IdkTryAgain.py";
			string command = "python ";
			command += idk;
			system(command.c_str());
		};
	
	}
}