import win32com.client
import datetime

now = datetime.datetime.now()

speaker = win32com.client.Dispatch("SAPI.SpVoice")

t = now.strftime("%I %M %p")

s = "It is " + t
speaker.Speak(s)