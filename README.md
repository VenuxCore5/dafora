DaFoRa Smart A.I.

### What is this repository for? ###

This repository is for a certain group of people looking to build an A.I.
Version *Alpha 0.1.0.1*
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Use Visual Studio (preferably Community) with C++ functionality
* A UNIX environment like Linux or MacOS could work as well but we do not guarantee it 
Note: WE DO NOT SUPPORT MAC, OR ANYTHING THAT IS NOT WINDOWS 10. USE AT YOUR OWN RISK.

### Contribution guidelines ###

* Make sure to commit your changes back here so others can work on it asap.
* Try to note what functionality you added and if the code actually works so we know what to do.
* Right now we need the right Python script to grab Weather data from OWM

### Notes ###

I have switched the project type from console app to Form application this requires getting used to.

### Who do I talk to? ###

* If you're here you know who to talk to

### Functionality ###

* Can do the four basic operator calculations with two numbers
* Telling Time
* Remembering your name and age
* Relaying weather (Only Ottawa weather atm/Update being worked on)
* Taking Notes
* DaFoRa's own Voice

### Future Updates ###

* GUI(NOTE: It is an abandoned project, so if you really want to tackle it, you're on your own) 
* Daily News 
* Taking Reminders and delivering them back at scheduled times
* Auto Completion of commands
* Personality and small talk
* Doing Math will soon become easier to do in AI! - Coming Soon!

# Owned by Amway Niggas #
## Real Niggas chose Amway ##